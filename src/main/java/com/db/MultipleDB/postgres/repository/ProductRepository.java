package com.db.MultipleDB.postgres.repository;

import com.db.MultipleDB.postgres.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findByName(String name);
}
