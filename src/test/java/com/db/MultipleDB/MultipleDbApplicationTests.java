package com.db.MultipleDB;

import com.db.MultipleDB.mysql.entity.User;
import com.db.MultipleDB.mysql.repository.UserRepository;
import com.db.MultipleDB.postgres.entity.Product;
import com.db.MultipleDB.postgres.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MultipleDbApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProductRepository productRepository;

	@Test
	public void dbTest(){
		System.out.println("testing...");

		User user = User.builder()
				.userName("Rocky")
				.email("Sahadat@ymail.com")
				.build();

		Product product = Product.builder()
				.name("Laptop")
				.description("without laptop nothing ...")
				.build();

		userRepository.save(user);
		productRepository.save(product);

		System.out.println("data save !!");
	}

	@Test
	public void getData(){
		userRepository.findAll().forEach(user -> System.out.println(user));
		productRepository.findAll().forEach(product -> System.out.println(product));
	}
}
